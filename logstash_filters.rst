Logstash Filtering and Patterns
===============================

Please see `the README <README.md>`__ for background information.

Contact: kayiwa@pobox.com

License: CCO

We'll put ephemera from today in this etherpad:

  https://etherpad.wikimedia.org/p/c4l16_logging

Logstash Filtering
------------------

Thus far we've pushed log sources to logstash without manipulating them in any way. While this was a useful exercise it hardly represents scenarios we will encounter. Messages with a small amount of metadata are rare and furthermore if left "as is" makes it difficult to do cross references, querying and analysis.

The first set of data we will manipulate will be apache logs. ::

Log into the beat VM and look in the ``/vagrant/examples/logs/`` directory. This is an example of apache logs routed through a BIG-IP f5 appliance with the IP of 192.168.2.6 ::

  66.249.66.101 192.168.2.6 - - [22/Feb/2016:17:11:59 -0500] "GET /index.php/JCTE/article/view/490/457 HTTP/1.1" 200 4753 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"

In its current form all of this would be pushed in the message field ::

  {
    "message" => "66.249.66.101 192.168.2.6 - - [22/Feb/2016:17:11:59 -0500] "GET /index.php/JCTE/article/view/490/457 HTTP/1.1" 200 4753 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
    "@timestamp" => "2016-02-20T01:29:42.416Z",
    "@version" => "1",
    "@host" => "beats",
    "@path" => "/vagrant/examples/logstash/logs/access.log"
    }

Logstash has a number of filters but for this workshop we will focus on the ``grok`` filter. This filter parses arbitrary text and structures it. It does this using patterns which are packaged regular expressions. Logstash ships with over 100 pre-existing patterns for most commonly used logs. Using ``grok`` patterns we can break down the ``message`` to use Apache's Combined Log Format.
