# Logging workshop agenda

Please see [the README for this repository](README.md) for times and expectations.

-----

This hands-on workshop will introduce attendees to the basics of ELK, a logging stack framework that can be used to parse, index and process logging. The ELK stack contains three distinct tools. [Elasticsearch](https://www.elastic.co/products/elasticsearch), [Logstash](https://www.elastic.co/products/logstash) and [Kibana](https://www.elastic.co/products/kibana) We will be looking at how you can integrate these to manage your logs centrally.

People new to ELK are welcome to attend. Familiarity with the UNIX Command Line is strongly recommended, and will make this class most useful

## Schedule

First hour:

* [Brief introduction to Elasticsearch](elastic_intro.rst)
* Working with Elasticsearch scaling

Second hour:

* [Brief introduction to logstash](logstash_intro.rst)
* [Working with filters and patterns](logstash_filters.rst)

Third hour:

* [Brief introduction to Kibana](kibana_intro.rst)
* Working with Kibana and Elasticsearch

Extra Time:

* Additional questions
