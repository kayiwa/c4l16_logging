# Buzzword Compliant Logging

March 7, 2016

Instructor: [Francis Kayiwa](https://twitter.com/kayiwa/)

## Overview

This hands-on workshop will introduce centralized logging using Elasticsearch, Logstash and Kibana. Attendees will look at a workflow that ships logs from a client to viewing it on the Kibana interface. A very generalized view of Elasticsearch, Logstash and their capabilities will be done.

People new to Elasticsearch, Logstash and Kibana are welcome to attend. I suggest that attendees have some familiarity with the UNIX Command Line.

### Software Installation etc.,

If you have a new laptop, we will be installing [Vagrant](https://www.vagrantup.com/) and [Virtualbox](https://www.virtualbox.org/) for our lab. Users of Windows are encouranged to download the ova image with an xubuntu image which we will use. While it is possible to attend this workshop with Microsoft Windows I haven't the resources to rigorously test this.

### Code of Conduct

Attendees at this workshop are expected to abide by the [Code4lib Code of Conduct](http://2016.code4lib.org/conduct.html). The local contact person for any concerns you may have during this workshop is.

In addition I will be applying the Hacker school [social rules](https://www.recurse.com/manual#sub-sec-social-rules) for the entirety of the workshop to foster a healthy learning environment.

### Workshop materials

The overall [agenda is here](AGENDA.md)
