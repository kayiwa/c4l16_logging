Elasticsearch
=============

Please see `the README <README.md>`__ for background information.

Contact: kayiwa@pobox.com

License: CCO

We'll put ephemera from today in this etherpad:

  https://etherpad.wikimedia.org/p/c4l16_logging

Introductory stuff
------------------

Who am I? Currently, background, interests...

My meta-goals for this workshop: hands-on exposure, with lots of rooms for questions. Please ask questions.

Note: I'm not an expert in Logstash, Elasticsearch or Kibana (please don't leave!)

Caveat: this is the second time I'm teaching a lot of this; please be gentle!

Starting level.

We have a `code of conduct! <http://2016.code4lib.org/conduct.html>`__ Basically, please be respectful of each other.

All of this material will be open (CC0). Use it as you wish!

Pretty please, ask questions!

Notes on sticky notes.

Bathrooms!

A few likely challenges:

* Linux, Mac, Windows... Anyone use BSD?

* Network bandwidth

** What are your goals and interests for this workshop? **

Before Getting Started
----------------------

Did everyone get an email from me?

The assumptions made in this class and a quick flyover on the tools we will be using.

Can you run this? ::

  vagrant version

and see something like this? ::

  Installed Version: 1.8.1
  ...

If that works you are most of the way there. Now can you run this? ::

  VBoxManage

and see something like this? ::

  Oracle VM VirtualBox Command Line Management Interface Version 5.0.14
  ...

We will be using these tools to build self contained labs that we can tear down with ease.

Bits and Bobs
-------------

A bit about the "buzz" We are using Vagrant for this lab mostly to allow rapid prototyping.

By the end of the workshop we will use ansible to stand up a reasonable Elasticsearch, Logstash and Kibana environment that can grow using a simplified ansible playbook

The keen eyed repo inspector will have noticed an additional tool... More on that later



Elasticsearch Installation
--------------------------

Try::

  vagrant up elasticsearch1 elasticsearch2 elasticsearch3

Then::

  vagrant ssh elasticsearch1

Contained in the ``/vagrant/examples/elasticsearch`` directory are steps to install elasticsearch on all your VMs. Follow the steps on ``elasticsearch_install.txt`` to install elasticsearch on all the three virtual machines. Look at the example `elasticsearch.yml` file. We will make changes to allow us to create a cluster later in the workshop when we talk about scaling. For now add the IP address and name of your node and copy that to `/etc/elasticsearch/`. When you are done start the elasticsearch server. ::

  sudo /usr/sbin/service elasticsearch restart

We will then install a simple web administration tool for elasticsearch on elasticsearch1 VM::

  cd /usr/share/elasticsearch/bin
  sudo -su elasticsearch ./plugin install lmenezes/elasticsearch-kopf/2.0

Launch your browser to point to http://10.0.15.21:9200, Make sure the plugin install correctly by going to http://10.0.15.21:9200/_plugin/kopf

Indexing
--------

Elasticsearch alone would require a workshop of its own. What follows is mostly a way to give us common language when thinking about logs. The act of adding/storing documents in Elasticsearch is called indexing. In Elasticsearch a document belongs to a type and those types **sigh** live in an index. A cluster and have multiple indices that contain multiple types. These types will hold multiple documents with multiple fields, ALL of which are indexed.

In order to index a first JSON object we make a PUT request to the REST API to a URL made up of the index name, type name and ID. That is: http://localhost:9200/<index>/<type>/[<id>]. Index and type are required while the id part is optional. If we don't specify an ID ElasticSearch will generate one for us. However, if we don't specify an id we should use POST instead of PUT.

The index name is arbitrary. If there isn't an index with that name on the server already one will be created using default configuration.

As for the type name it too is arbitrary. It serves several purposes, including:

* Each type has its own ID space.

* Different types can have different mappings ("schema" that defines how properties/fields should be indexed).

* Although it's possible, and common, to search over multiple types, it's easy to search only for one or more specific type(s).

Let's index something! We can put just about anything into our index as long as it can be represented as a single JSON object. In this tutorial we'll be indexing and searching for Arsenal football players. Here's a classic one: ::

  {
    "first_name" : "Dennis",
    "last_name" : "Bergkamp",
    "age" : 46,
    "Nationality" : "Dutch",
  }

To index that we decide on an index name ("squad"), a type name ("player") and an id ("1") and make a request following the pattern described above with the JSON object in the body. ::

  curl -XPUT "http://10.0.15.21:9200/squad/player/1" -d'
  {
  "first_name" : "Dennis",
  "last_name" : "Bergkamp",
  "age" : 46,
  "Nationality" : "Dutch"
  }

We will use curl in the first example and then use the installed plugin for the next few. After executing the request we receive a response from Elasticsearch in the form of a JSON object.

Now that we've got Dennis in our index let's look at how we can update it, adding a list of positions to it and correcting Dennis' nationality. In order to do that we simply index it again using the same ID. In other words, we make the exact same indexing request as as before but with an extended JSON object containing positions. Use the plugin to add the following. ::

  curl -XPUT "http://10.0.15.21:9200/squad/player/1" -d'
    {
      "first_name" : "Dennis",
      "last_name" : "Bergkamp",
      "age" : 46,
      "Nationality" : "Netherlands",
      "position" : "forward"
    }

The response from Elasticsearch is the same as before with one difference, the _version property in the result object has value two instead of one. Let's add a few more. Elasticsearch offers a bulk option for adding documents that is beyond the scope of this class. Otherwise add the following players to the Arsenal squad. ::

  curl -XPUT "http://10.0.15.21:9200/squad/player/2" -d'
    {
      "first_name" : "Robin",
      "last_name" : "Van Persie",
      "age" : 33,
      "Nationality" : "Netherlands",
      "position" : "forward"
    }
  curl -XPUT "http://10.0.15.21:9200/squad/player/3" -d'
    {
      "first_name" : "Patrick",
      "last_name" : "Vieira",
      "age" : 42,
      "Nationality" : "France",
      "position" : "midfield"
    }
  curl -XPUT "http://10.0.15.21:9200/squad/player/4" -d'
    {
      "first_name" : "Jens",
      "last_name" : "Lehmann",
      "age" : 42,
      "Nationality" : "Germany",
      "position" : "Goalkeeper"
    }

Retrieving a Document
---------------------

With all this newly indexed data. We can retrieve them using the HTTP GET request by specifying the address of the document - the index, type and ID. ::

  GET /squad/player/4

and the response contains the metadata about the document, and Jen Lehmann's original JSON document as the _source field: ::

  {
    "_index": "squad",
    "_type": "player",
    "_id": "4",
    "_version": 1,
    "found": true,
    "_source": { -
      "first_name": "Jens",
      "last_name": "Lehmann",
      "age": 43,
      "Nationality": "Germany",
      "Position": [ -
        "Goalkeeper"
      ]
    }
  }

Searching
---------

Now that we have put some players into our index, let's see if we can find them again by searching. In order to search with Elasticsearch we use the _search endpoint, optionally with an index and type. That is, we make requests to an URL following this pattern: <index>/<type>/_search where index and type are both optional.

In other words, in order to search for our movies we can make POST requests to either of the following URLs:

* http://10.0.15.21:9200/_search - Search across all indexes and all types.

* http://10.0.15.21:9200/squad/_search - Search across all types in the movies index.

* http://10.0.15.21:9200/squad/player/_search - Search explicitly for documents of type squad within the player index.

We only have a single index and a single type which one we use doesn't matter. We'll use the last URL for our example. ::

  GET /squad/player/_search

The response will include all the documents in the hits array. Elasticsearch defaults to returning the top 10 results. ::

  {
    {
      "took": 3,
      "timed_out": false,
      "_shards": {
      "total": 5,
      "successful": 5,
      "failed": 0
    },
    "hits": {
      "total": 4,
      "max_score": 1,
      "hits": [
      {
         "_index": "squad",
         "_type": "player",
         "_id": "2",
         "_score": 1,
         "_source": {
           "first_name": "Robin",
           "last_name": "Van Persie",
           "age": 33,
           "Nationality": "Netherlands",
           "Position": [
             "Forward"
           ]
         }
       },
       ...

Next, let's try searching for Arsenal's Dutch players. To do this, we'll use the light weight search method, often referred to as a `query-string` search, since we pass the search as a URL query-string parameter. ::

   GET /squad/player/_search?q=Nationality:Netherlands

We use the same `_search` endpoint in the path, and we add the query itself in the `q=` parameter. The results will come back with all the Dutch players. ::

   {
     {
        "took": 32,
        "timed_out": false,
        "_shards": {
        "total": 5,
        "successful": 5,
        "failed": 0
        },
        "hits": {
          "total": 2,
          "max_score": 1,
          "hits": [
          {
            "_index": "squad",
            "_type": "player",
            "_id": "2",
            "_score": 1,
            "_source": {
              "first_name": "Dennis",
              "last_name": "Bergkamp",
              "age": 46,
              "Nationality": "Netherlands",
              "Position": [
                "Forward"
              ]
            }
            ...

The above search is similar to this one below using `POST` ::

  POST /squad/player/_search" -d'
  {
      "query": {
          "query_string": {
              "query": "Netherlands"
          }
      }
  }'

We can perform the same search above but narrow it down to one field. Elasticsearch defaults to search all fields. ::

  POST /squad/player/_search" -d'
  {
      "query": {
          "query_string": {
              "query": "Netherlands",
              "fields": ["Nationality"]
          }
      }
  }

Elasticsearch capabilities are further demonstrated in `their documentation <https://www.elastic.co/guide/en/elasticsearch/guide/current/index.html>`_ which is beyond the scope of this workshop. I do encourage giving it a look if you write applications etc.,
