Elasticsearch
=============

Please see `the README <README.md>`__ for background information.

Contact: kayiwa@pobox.com

License: CCO

We'll put ephemera from today in this etherpad:

  https://etherpad.wikimedia.org/p/c4l16_logging

Logstash Installation
---------------------

Launch your VM ::

  vagrant up logstash

Log into the VM ::

  vagrant ssh logstash

Contained in the  ``/vagrant/examples/logstash`` directory are the steps we will use to install logstash on the VM. Follow the steps in the ``logstash_install.txt`` file as root to install the software. We will use the simple configuration file in the same directory to run logstash interactively. When done start the logstash service ::

  sudo /opt/logstash/bin/logstash agent --debug -f /vagrant/examples/logstash/configs/simple_logstash.conf

Depending on the speed of your VM this will take a few seconds to launch. Since our input is expecting STDIN enter a ``hello world`` message and hit the ENTER button to send it to logstash. You will see an approximation of the message below between the debug messages the we enable in the output ::

  {
         "message" => "hello world",
         "@version" => "1",
         "@timestamp" => "2016-02-20T16:01:33.401Z",
         "host" => "logstash"
  }

Our input has resulted into an ``info`` level message from Logstash printed as a hash.

Shipping Events
---------------

Logstash ships with a number of useful plugins by default. We have already seen the ``stdin`` and the ``stdout`` ones in our example in the previous section. In addition to the default one can install additional plugins which we will do shortly. First to see a list of the default packaged plugins run the following command from the ``/opt/logstash/bin`` directory. ::

  sudo /opt/logstash/bin/plugin list

You will see an list of some of the default packaged ones. ::

  logstash-codec-collectd
  logstash-codec-dots
  logstash-codec-edn
  logstash-codec-edn_lines
  ...

You will notice that the elasticsearch output is bundled by default ::

 sudo /opt/logstash/bin/plugin list | grep elasticsearch

For the rest of the workshop we will be working with the beats plugin so lets install it on the logstash server. ::

  sudo /opt/logstash/bin/plugin install logstash-input-beats

In the ``/vagrant/examples/logstash/configs`` is the ``filter_free.conf`` file. We will copy this file for our initial logstash install.Let's examine the make up of the file to elaborate what is going on. ::

  cat /vagrant/examples/logstash/configs/filter_free.conf
  input {
	   beats {
		   port => 5044
	   }
  }

  output {
    stdout { }
	  elasticsearch {
		  hosts => ["10.0.15.21"]
	  }
  }

The file has the input section which will accept content via the just installed beats plugin on port 5044. The content from the file will be processed "as is" and sent to the elasticsearch host of ``10.0.15.21`` Copy this file and restart the logstash. ::

  sudo cp /vagrant/examples/logstash/configs/filter_free.conf /etc/logstash/conf.d/filter_free.conf
  sudo service logstash restart

With this installed we will take a detour look at the Filebeat the log shipper. Exit from the logstash VM and start the beat one. ::

  vagrant@logstash exit
  logout
  Connection to 127.0.0.1 closed
  ⋊> ~/D/n/c/vagrant_machines on master ⨯
  vagrant up beat

Filebeat Detour
---------------

Right now we have an elasticsearch cluster configured. We have a logstash central server set up. The next step is to ship data to the logstash central server and have the data sent to the elasticsearch cluster. We will install Filebeat with the steps in the ``/vagrant/examples/logstash/filebeat/filebeat_install.txt`` file. Copy the ``filebeat.yml`` to ``/etc/filebeat/`` of the beat VM. Filebeat is a golang based small and lightweight client which replaces the java based lumberjack and logstash-forwarder for sending messages to logstash. Filebeat is configured via the YAML file we just copied above. ::

  filebeat:
    prospectors:
      -
      paths:
        - /var/log/*.log
      input_type: log
      document_type: beat
    registry: /var/lib/filebeat/registry
  output:
    logstash:
      hosts: ["10.0.15.10:5044"]
  logging:
    to_files: true
    files:
      path: /var/log/filebeat
      name: filebeat
      rotateeverybytes: 10485760
    level: error

The ``filebeat.yml`` file is divided into these three stanzas that concern us. 

* prospectors: which tells filebeat which files to gather

* output: which tells filebeat where to send the files

* logging: which controls filebeat's own logging

We can now restart the filebeat service ::

  sudo service filebeat restart

If you point your browser to the kopf plugin URI from the elasticsearch section of this workshop http://10.0.15.21:9200/_plugin/kopf you will find data from the beat VM. Use the Elasticsearch query language to search for ``ssh`` from the REST API section of the plugin. In the Kibana section of the workshop we will look at how to provide better visualization of the collected data. For now let's look at how we can use filters and patterns in Logstash.
