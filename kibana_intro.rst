Elasticsearch
=============

Please see `the README <README.md>`__ for background information.

Contact: kayiwa@pobox.com

License: CCO

We'll put ephemera from today in this etherpad:

  https://etherpad.wikimedia.org/p/c4l16_logging

Kibana Installation
-------------------

Launch your VM ::

  vagrant up kibana

Log into the VM ::

  vagrant ssh kibana

In the ``/vagrant/examples/kibana`` directory like in the last ``logstash`` and ``elasticsearch`` examples is a file named ``kibana_install.txt`` which contains the steps needed to install the Kibana software. Look at the contents of the ``kibana.yml`` file and modify them accordingly. Copy this file to ``/opt/kibana/config`` and start the kibana service with ``sudo service kibana start``

We can not look at the contents of the elasticsearch cluster at http://10.0.15.11:5601
